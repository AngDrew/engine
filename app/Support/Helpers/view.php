<?php
/**
 * @author <rendy, {13/08/17}-{22:02}>
 */
if (! function_exists('set_active')) {
    function set_active($name, $activeClass = 'is-active')
    {
        return strpos(\Illuminate\Support\Facades\Route::currentRouteName(), $name) !== false ? $activeClass : '';
    }
}

if (! function_exists('set_error')) {
    function set_error($input, $invalidClass = 'is-danger')
    {
        $errors = session('errors');
        if (isset($errors)) {
            return $errors->has($input) ? $invalidClass : '';
        }
    }
}

if (! function_exists('show_error')) {
    function show_error($input, $class = 'help is-danger')
    {
        $errors = session('errors');
        $string = isset($errors) ? "<p class='$class'> {$errors->first($input)} </p>" : '';

        return new \Illuminate\Support\HtmlString($string);
    }
}

if (! function_exists('set_selected')) {
    function set_selected($checker1, $checker2)
    {
        return $checker1 == $checker2 ? 'selected' : '';
    }
}

if (! function_exists('set_checked')) {
    function set_checked($checker1, $checker2)
    {
        return $checker1 == $checker2 ? 'checked' : '';
    }
}

if (! function_exists('set_tab_active')) {
    function set_tab_active($checker1, $checker2)
    {
        return $checker1 == $checker2 ? 'active' : '';
    }
}

if (! function_exists('words')) {
    /**
     * Limit the number of words in a string.
     *
     * @param  string  $value
     * @param  int     $words
     * @param  string  $end
     * @return string
     */
    function words($value, $words = 100, $end = '...')
    {
        return \Illuminate\Support\Str::words($value, $words, $end);
    }
}
