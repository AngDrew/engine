<?php

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Builder;
use Webpatser\Uuid\Uuid;
use App\Models\Account\Admin;
use Illuminate\Database\Eloquent\Model;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 29/04/17 , 18:59
 */
class Media extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });

        self::deleting(function (self $model) {
            app('filesystem')->disk('media')->delete($model->getAttribute('src'));
        });

        self::addGlobalScope(function (Builder $builder) {
            return $builder->orderBy('type');
        });
    }

    protected $table = 'media';

    public $incrementing = false;

    protected $primaryKey = 'uuid';

    protected $fillable = [
        'title' , 'src', 'type'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function author()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id');
    }

    public function edition()
    {
        return $this->belongsTo(Edition::class, 'edition_id', 'id');
    }

}
