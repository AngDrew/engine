<?php

namespace App\Models\Contents;

use App\Models\Account\Admin;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Core\HashId\Eloquent\HashableId;
use Illuminate\Support\Facades\Storage;

class Edition extends Model
{
    use HashableId;

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->slug = str_slug($model->name);
        });

        self::addGlobalScope(function (Builder $builder) {
            return $builder->orderBy('created_at', 'desc');
        });
    }

    protected $table = 'editions';

    protected $with = ['languages'];

    protected $appends = ['hash', 'cover'];

    protected $fillable = [
        'name', 'released',
    ];

    protected $casts = [
        'released' => 'boolean',
    ];

    public function style()
    {
        return $this->hasMany(Style::class, 'edition_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'id');
    }

    public function pages()
    {
        return $this->hasMany(Page::class, 'edition_id', 'id');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class, 'edition_language');
    }

    public function media()
    {
        return $this->hasMany(Media::class, 'edition_id', 'id');
    }

    public function scopeReleased (Builder $query)
    {
        return $query->where('released', true);
    }

    public function getCoverAttribute()
    {
        $firstPage = $this->pages()
            ->where('pages.order', 1)
            ->first()
            ->image()
            ->first();

        if (! is_null($firstPage)) {
            return $firstPage->thumb["id"];
        }

        return $firstPage;
    }

    public function getPdfAttribute()
    {
        return $this
            ->media()
            ->where("type", "pdf")
            ->select(\DB::raw("substr(title, -2, 5) as lang"), "uuid as src")
            ->get();

    }

    public function getAttachedMediaAttribute ()
    {
        return $this
            ->media()
            ->whereNotIn("type", ["apk", "pdf", "image"])
            ->select("src", "type", "title")
            ->get();
    }
}
