<?php

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Page extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::deleting(function (self $page) {
            $page->image()->delete();
            $page->contents()->delete();
        });

        self::addGlobalScope(function (Builder $builder) {
            return $builder->orderBy('order');
        });
    }

    protected $table = 'pages';

    protected $fillable = [
        'title',
        'order',
    ];

    protected $with = ['image'];

    protected $appends = ['contents'];

    public function edition()
    {
        return $this->belongsTo(Edition::class, 'edition_id', 'id');
    }

    public function image()
    {
        return $this->hasOne(Image::class, 'page_id', 'id');
    }

    public function contents()
    {
        return $this->hasMany(Content::class, 'page_id', 'id');
    }

    public function getContentsAttribute()
    {
        $data = [];
        foreach ($this->getAttribute('edition')->languages as $language) {
            $content = $this->contents()->where('language_id', $language->id);
            $data[$language->code] = '';
            if ($content->count() !== 0) {
                $data[$language->code] = ($content->first()->body);
            }
        }

        return $data;
    }

    public function scopeReleased (Builder $query)
    {
        return $query->where('released', 1);
    }
}
