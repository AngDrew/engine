<?php

namespace App\Core\HashId\Eloquent;

use App\Core\HashId\Repository as HashRepository;

/**
 * Eloquent Model Hashable Id.
 *
 * @author      veelasky <veelasky@gmail.com>
 */
trait HashableId
{
    /**
     * Get Model by hashed key.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $hash
     *
     * @return mixed
     */
    public function scopeByHash($query, $hash)
    {
        return $query->where($this->getKeyName(), $this->hashIds()->decode($hash))
            ->first();
    }
    /**
     * Get Model by hashed key or fail.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $hash
     *
     * @return mixed
     */
    public function scopeByHashOrFail($query, $hash)
    {
        return $query->where($this->getKeyName(), $this->hashIds()->decode($hash))
            ->firstOrFail();
    }
    /**
     * Get Hash Attribute.
     *
     * @return string
     */
    public function getHashAttribute()
    {
        return $this->hashIds()
            ->encode($this->getOriginal($this->getKeyName()));
    }
    /**
     * Get Hashids Implementation.
     *
     * @return \Hashids\Hashids
     */
    protected function hashIds()
    {
        /*
         * HashId Repository class.
         *
         * @var \Maclev\Core\HashId\Repository
         */
        $repository = app(HashRepository::class);
        // if it already exists on repository let's throw them
        if ($repository->has($this->getTable())) {
            return $repository->get($this->getTable());
        }
        $salted = substr(strrev(self::class), 0, 4).substr(env('APP_KEY'), 0, 4);
        // ... create a new hashid instance if it not existed
        $hash = $repository->make($this->getTable(), $salted);

        return $hash;
    }
}
