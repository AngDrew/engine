<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class EditionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'  => $this->name,
            'cover' => $this->cover,
            'pdf'   => $this->pdf,
            'media' => $this->attachedMedia,
        ];
    }
}
