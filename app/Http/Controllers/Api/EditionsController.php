<?php

namespace App\Http\Controllers\Api;

use App\Models\Contents\Edition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\EditionResource;

class EditionsController extends Controller
{
    public function index ()
    {
        $editions = Edition::released()->latest()->get();
        return EditionResource::collection($editions);
    }
}
