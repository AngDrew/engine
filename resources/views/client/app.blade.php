<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>Majalah Semanggi</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  @stack('styles')
</head>
<body>
  @yield('content')
  <script src="{{ asset('js/app.js') }}"></script>
  @stack('scripts')
</body>
</html>
