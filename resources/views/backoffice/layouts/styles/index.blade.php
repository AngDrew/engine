@extends('backoffice.layouts.app')

@section('header')
  Dasbor
@endsection

@push('modals')
  <div class="modal" id="modal-add-stylesheet">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <h2>Tambah Stylesheet</h2>
      </header>
      <section class="modal-card-body">
        <form action="{{ route('admin.style.store', [ 'edition' => $edition->hash ]) }}" method="post" id="store-stylesheet">
          {{ csrf_field() }}
          <div class="field">
            <label class="label">Nama Stylesheet</label>
            <div class="control">
              <input class="input" type="text" placeholder="Cover" name="name" value="{{ old('name') }}">
            </div>
          </div>
          <div class="field">
            <label class="label" for="code-css">Isi</label>
            <div class="control">
              <textarea class="textarea css-texteditor" name="body" id="code-css" cols="30" rows="10">{{ old('body') }}</textarea>
            </div>
          </div>
        </form>
      </section>
      <footer class="modal-card-foot">
        <button class="button is-success" onclick="document.getElementById('store-stylesheet').submit()">Simpan</button>
        <button class="button modal-dismiss">Batal</button>
      </footer>
    </div>
  </div>
@endpush

@section('content')
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.editions.list')
      </div>
    </div>
  </div>
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.styles.list')
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script src="{{ asset('/js/texteditor.js') }}"></script>
@endpush
