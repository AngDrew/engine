  <div class="modal" id="modal-add-edition">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <p class="modal-card-title">Tambah Edisi</p>
        <button class="modal-dismiss" aria-label="close"></button>
      </header>
      <section class="modal-card-body">
        <form action="{{ route('admin.edition.store') }}" method="post" id="store-edition">
          {{ csrf_field() }}
          <div class="column field">
            <label class="label">Nama Edisi</label>
            <div class="control">
              <input class="input" type="text" placeholder="Wonderful Magazine" name="name">
            </div>
          </div>
          <div class="column is-3">
            <div id="languages-form">
              <label class="label">Bahasa</label>
              <div class="select is-multiple">
                <label>
                  <select multiple size="{{ count($languages) }}" name="languages[]" required>
                    @foreach($languages as $language)
                    <option value="{{ $language->id }}">{{ $language->code }}</option>
                    @endforeach
                  </select>
                </label>
              </div>
            </div>
          </div>
        </form>
      </section>
      <footer class="modal-card-foot">
        <button class="button is-success" onclick="document.getElementById('store-edition').submit()">Simpan</button>
        <button class="button modal-dismiss">Batal</button>
      </footer>
    </div>
  </div>
  <div class="modal" id="modal-list-edition">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <p class="modal-card-title">Daftar Edisi</p>
        <button class="modal-dismiss" aria-label="close"></button>
      </header>
      <section class="modal-card-body">
        @include('backoffice.components.editions.list')
      </section>
    </div>
  </div>
  @stack('modals')
