<h3>Halaman Edisi : {{ $edition->name }}</h3>
<a href="javascript:void(0)" class="button is-link is-pulled-right modal-button" data-target="modal-add-page">
  <span class="ion-plus-round"></span>&nbsp;&nbsp; Halaman
</a>
<div class="is-clearfix" style="margin-bottom: 20px"></div>
@each('backoffice.components.pages.list_item', $pages, 'page', 'backoffice.components.pages.list_empty')
{{ $pages->links() }}
