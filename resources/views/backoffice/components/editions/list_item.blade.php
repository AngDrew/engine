<tr>
  <td>{{ $edition->name }}</td>
  <td>{{ $edition->languages->implode('code', ', ') }}</td>
  <td>
    @if( $edition->released )
      <span class="tag is-link">Telah Dirilis</span>
    @else
      <span class="tag is-danger">Belum Dirilis</span>
    @endif
  </td>
  <td>
    <form action="{{ route('admin.edition.destroy', [ 'edition' => $edition->hash ]) }}"
          style="display: none;" method="post" id="delete-form-{{ $edition->hash }}">
      {{ csrf_field() }}
      {{ method_field('delete') }}
    </form>
    <a href="javascript:void(0)" class="modal-button" data-target="modal-option-{{ $edition->hash }}">
            <span class="icon has-text-info">
              <span class="ion-ios-paper-outline"></span>
            </span>
    </a>
    <a href="{{ route('admin.edition.edit', [ 'id' => $edition->hash ]) }}">
            <span class="icon has-text-warning">
              <span class="ion-edit"></span>
            </span>
    </a>
    <a href="javascript:void(0)" class="swal-warn" data-target="#delete-form-{{ $edition->hash }}"
       data-title="Hapus {{ $edition->name }} ? " data-subtitle="Data tidak bisa kembali">
            <span class="icon has-text-danger">
              <span class="ion-trash-b"></span>
            </span>
    </a>
  </td>
</tr>

@push('modals')
  <div class="modal" id="modal-option-{{ $edition->hash }}">
    <div class="modal-background"></div>
    <div class="modal-card">
      <section class="modal-card-body">
        <a class="button is-danger is-inverted"
           href="{{ route('admin.media.index', [ 'edition' => $edition->hash ]) }}">Media</a>
        <a class="button is-success is-inverted"
           href="{{ route('admin.style.index', [ 'edition' => $edition->hash ]) }}">Stylesheet</a>
        <a class="button is-link is-inverted"
           href="{{ route('admin.page.index', [ 'edition' => $edition->hash ]) }}">Halaman</a>
      </section>
    </div>
  </div>
@endpush
