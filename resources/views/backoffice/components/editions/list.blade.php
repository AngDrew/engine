<table class="table is-fullwidth">
  <thead>
  <tr>
    <th>Nama</th>
    <th>Bahasa</th>
    <th>Status</th>
    <th></th>
  </tr>
  </thead>
  <tbody>
  @each('backoffice.components.editions.list_item', $editions, 'edition', 'backoffice.components.editions.list_empty')
  </tbody>
</table>
