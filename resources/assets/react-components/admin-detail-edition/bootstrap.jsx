import React from 'react';
import Page from './containers/page';
import Content from './containers/content';

export default class App extends React.Component {
  constructor (props) {
    super(props);

    this.state = {};
    for (let prop in props) {
      this.state[prop] = JSON.parse(props[prop])
    }

    this.handleBackgroundChange = this.handleBackgroundChange.bind(this)
  }

  componentDidMount () {

  }

  handleBackgroundChange (page) {
		this.setState({ page : page })
	}

  render () {
    return (
      <div className="columns is-multiline is-desktop">
        <Page onBackgroundChange={this.handleBackgroundChange} url={this.state.url.image} />
        <Content languages={this.state.edition.languages} page={this.state.page} url={this.state.url.content}
								 imageUrl={this.state.url['imageUrl']}/>
      </div>
    )
  }
}
