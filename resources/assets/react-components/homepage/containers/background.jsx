import React from 'react';
import ParticleConfig from '../config/particles';
import Snow from 'react-snow-effect';

export default class Background extends React.Component{
	constructor(props){
			super(props);
	}

	componentDidMount() {
		particlesJS.load('particle-bg','/js/particlesjs-config.json');
	}

	render(){
		return (
			<div id="background" style={{ zIndex: 0 }}>
				<div className="gradient"></div>
				<div id="particle-bg" style={{ position: 'fixed', width: '100%', height: '100vh' }}></div>
				{/*<Snow/>*/}
				{/*<div className="image"></div>*/}
			</div>
		);
	}

}
