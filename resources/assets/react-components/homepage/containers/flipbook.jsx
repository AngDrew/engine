import React from 'react';
import Draggable from 'react-draggable';
import $ from 'jquery';
import 'turn.js';
import { init, turned } from '../actions/flipbook';
import { id, size, element } from '../constants/FlipBook';
import { connect } from 'react-redux';

class FlipBook extends React.Component{

    constructor(props){
      super(props);

      this.state = {
      	position: {x: 0, y:0}
      };

      this.handleDrag = this.handleDrag.bind(this);
      this.handleTurned = this.handleTurned.bind(this);
    }

    static _createMarkup(html){
      return { __html : html };
    }

    static _renderDiv(item, index, lang='id'){
      let style = {
        backgroundImage : "url(\""+ window.data.url.image.replace('uuid', item.image.uuid) +"\")",
        backgroundSize  : "100% 100%",
        width : '380px',
				height: '550px',
				position: 'relative'
      };
      return (
        <div key={ index } style={ style }
          dangerouslySetInnerHTML={FlipBook._createMarkup(item.contents[lang])}/>
      );
    }

    componentDidUpdate() {
      if (this.props.magazine.data.pages !== undefined) {
      	if (!this.props.magazine.initialized) {
      		const defaultOptions = {
			        width : size.width,
			        height: size.height,
			        //autoCenter : true,
			        when : { turning : this.handleTurned }
			    };
			    $(element).turn(defaultOptions)
      		this.props.dispatch(init())
      	}
      }
    }

    handleDrag(e, data) {
    	this.setState({ position: {x: data.x, y: data.y} });
    }

    handleTurned(a, b, c) {
    	this.props.dispatch(turned(b));
    }

    render(){
      let page = [];
      if (this.props.magazine.data.pages !== undefined) {
	      this.props.magazine.data.pages.map((item, index) => {
	        page.push(FlipBook._renderDiv(item, index, this.props.setting.lang));
	      });
      }
      let position = this.state.position ;
      if (!(this.props.magazine.scale > 1)) {
      	position = {x: 0, y:0}
      }
      return (
        <div id="flipBook-container"
		         style={ Object.assign({transform : "scale("+ this.props.magazine.scale +")"}, size) }
		         className="center content">
        	<Draggable disabled={ !(this.props.magazine.scale > 1) }
        						 position={ position }
        						 onDrag={ this.handleDrag }>
	          <div id={ id }>
	              { page }
	          </div>
          </Draggable>
        </div>
      );
    }
}

const mapStateToProps = (state) => {
	return state
};

export default connect(mapStateToProps)(FlipBook)
