import React from 'react';
import ReactDOM from 'react-dom';
import App from './bootstrap';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducers';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { FETCH_DATA, RECEIVE_DATA } from './constants/Magazine';
import swal from 'sweetalert';

const notify = store => next => action => {
	switch(action.type) {
		case FETCH_DATA :
			swal({
				title               : "Loading...",
				buttons             : false,
				closeOnClickOutside : false
			}).then(() => {});
			break;
		case RECEIVE_DATA :
			swal.close()
			break;
	}
	return next(action)
};

const middleware = [thunk, notify, createLogger()];

let store = createStore(reducer, applyMiddleware(...middleware));
let root = document.getElementById('root');

// Let the application begin
ReactDOM.render(
		<Provider store={store} >
			<App/>
		</Provider>
	, root);
