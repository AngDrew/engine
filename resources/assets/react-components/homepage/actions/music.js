export const playToggle = () => ({
	type : 'PLAY_TOGGLE'
})

export const changeAudio = (key) => ({
	type : 'CHANGE_AUDIO',
	selected : key
})
