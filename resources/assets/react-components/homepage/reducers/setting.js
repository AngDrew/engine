import {CHANGE_SETTING} from "../constants/Setting";

const defaultState = {
  lang : 'id',
  simpleMode : false,
};

export default (state = defaultState, action) => {
    switch (action.type) {
      case CHANGE_SETTING:

        break;
      case 'CHANGE_LANG':
      	return Object.assign({}, state, {lang: action.code});
      default :
      	return state;
      break;
    }
}
