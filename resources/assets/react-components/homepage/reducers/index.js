import {combineReducers} from 'redux';
import magazine from './magazine';
import setting from './setting';
import music from './music';

export default combineReducers({
  magazine, setting, music
});
