import { FETCH_DATA, INVALID_DATA, RECEIVE_DATA,
				 RENDER, ZOOMIN, ZOOMOUT, ZOOMDEFAULT } from "../constants/Magazine";

const defaultState = {
  isFetching: false,
  didInvalidate: false,
  data: [],
  scale: 1,
  initialized: false,
  page: 1,
  pattern: []
};

let scale;

let flip_audio = new Audio('pageflip.mp3');

export default (state = defaultState, action) => {
  switch (action.type){
    case INVALID_DATA :
      return Object.assign({}, state, {didInvalidate: true});
      break;
    case FETCH_DATA :
      return Object.assign({}, state, {isFetching: true});
      break;
    case RECEIVE_DATA :
      return Object.assign({}, state, {
          isFetching: false,
          didInvalidate: false,
          data: action.data
      });
      break;
    case "INIT_MAGAZINE" :
    	let pages = state.data.pages;
			let length = (pages.length - 1);
			let pattern = [];
	    pages.map((item, index) => {
	    	if (index == 0 || index == length) {
	    		pattern.push([index]);
	    	} else {
	    		if (index % 2 == 1) {
	    			pattern.push([index, (index+1)]);
	      	}
	    	}
	    });
    	return Object.assign({}, state, {
          initialized: true,
          pattern: pattern
      });
      break;
    case "TURNED" :
    	window.location.hash = "?page=" + action.page;
    	flip_audio.play();
    	return Object.assign({}, state, {
          page: action.page
      });
    	break;
    case ZOOMDEFAULT :
    	return Object.assign({}, state, {
          scale: 1
      });
    	break;
    case ZOOMIN :
    	scale = state.scale;
    	if (scale + 0.2 < 1.8) {
    		scale += 0.2
    	}
    	return Object.assign({}, state, {
          scale: scale
      });
    	break;
    case ZOOMOUT :
	    scale = state.scale;
    	if (scale - 0.2 > 0.2) {
    		scale -= 0.2
    	}
    	return Object.assign({}, state, {
          scale: scale
      });
    	break;
    case 'ZOOMTO' :
    	scale = (action.percentage / 100)
    	return Object.assign({}, state, {
          scale: scale
      });
      break;
    case 'PUBLISH_PATTERN' :
    	return Object.assign({}, state, {
    		pattern : action.pattern
    	});
    	break;
    default :
      return state;
      break;
  }
}
