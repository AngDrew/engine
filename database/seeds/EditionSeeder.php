<?php

use Illuminate\Database\Seeder;

class EditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $edition = new \App\Models\Contents\Edition();
        $edition->fill(['name' => 'Semanggi magazine']);
        $edition->author()->associate(\App\Models\Account\Admin::find(1));
        $edition->save();
        $edition->languages()->sync([1, 2]);
    }
}
