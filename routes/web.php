<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'as' => 'app.',
], function () {
    Route::get('/', 'MagazineController@index')->name('index');

    Route::get('/css/{editionSlug}/compiled.css', 'MagazineController@compiledCss')->name('css');
    Route::get('images/{image}', 'MagazineController@getImages')->name('images');
    Route::get('media/{medium}', 'MagazineController@getMedia')->name('media');

    Route::get('magazine/{editionSlug}', 'MagazineController@getEdition')->name('get-edition');
    Route::get('/{editionSlug}', 'MagazineController@view')->name('view');
});

Route::group(['namespace' => 'Auth', 'prefix' => 'auth', 'as' => 'auth.'], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('show-login');
    Route::post('login/{guard?}', 'LoginController@login')->name('login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::group([
    'middleware' => 'auth:admin',
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::resource('edition', 'OrganizeController')->only(['store', 'edit', 'update', 'destroy']);

    Route::group(['prefix' => '{edition}'], function () {
        Route::resource('page', 'PageController')->except(['edit']);
        // Another route for detail page
        Route::group(['as' => 'page.', 'prefix' => 'page/{page}'], function () {
            Route::post('update-image', 'PageController@updateImage')->name('update-image');
            Route::post('update-content', 'PageController@updateContent')->name('update-content');
        });

        Route::resource('media', 'MediaController')->only(['index', 'store', 'update', 'destroy']);
        Route::get("/pdf/create", "MediaController@createPdf")->name("media.create-pdf");
        Route::resource('style', 'StyleController')->only(['index', 'store', 'update', 'destroy']);
    });
});

